#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>
#include <sys_clock.h>
#include <irq.h>
#include <misc/printk.h>
#include <misc/util.h>
#include <limits.h>
#include "config.h"
#include "tb_pubsub.h"
#include <stdlib.h>//required to use 'rand()'

#define BTN_PORT SW0_GPIO_NAME
#define SIMULATED_REQUESTER_GPIO_PIN SW0_GPIO_PIN
#define SLOTS_COUNT 4
#define SIMULATED_REQUEST_SIGNAL 1


#define PORT	CONFIG_GPIO_NRF5_P0_DEV_NAME
#define PIN_TRIG	11
#define PIN_ECHO	12


/* Macro */
#define HW_CYCLES_TO_USEC(__hw_cycle__) \
        ( \
                ((uint64_t)(__hw_cycle__) * (uint64_t)sys_clock_us_per_tick) / \
                ((uint64_t)sys_clock_hw_cycles_per_tick) \
        )


/* Inline functions */
static inline uint32_t time_delta(uint32_t ts, uint32_t t)
{
        return (t >= ts) ? (t - ts) : (ULONG_MAX - ts + t);
}

struct device *dev;


u32_t slots_state[SLOTS_COUNT] = {
    0,
    0,
    0,
    0
};

u32_t vehicle_id;

struct device *btn_dev;
struct gpio_callback btn_callback;

int btn_alert_handler(struct k_alert *alert);
K_ALERT_DEFINE(btn_alert, btn_alert_handler, 10);

void btn_handler(struct device *port, struct gpio_callback *cb, u32_t pins)
{
    /* Context: interrupt handler */
	printk("Signalling alert!\n");
	k_alert_send(&btn_alert);
}



// Send 1-wire reset pulse
u32_t hcsr04_find_occupancy()
{
	u32_t value = 0;
	u32_t result = 0;
	u32_t nanoseconds_spent = 0;
	u32_t cycles_spent =0;
	double dist_val;
	
	unsigned int key;

	key = irq_lock();
	
    
    gpio_pin_configure(dev, PIN_TRIG, GPIO_DIR_OUT);
	gpio_pin_write(dev, PIN_TRIG, 0);
	k_busy_wait(30);
	gpio_pin_configure(dev, PIN_TRIG, GPIO_DIR_OUT);
	gpio_pin_write(dev, PIN_TRIG, 1);
	k_busy_wait(10);
	gpio_pin_configure(dev, PIN_TRIG, GPIO_DIR_OUT);
	gpio_pin_write(dev, PIN_TRIG, 0);

	do {    
		    gpio_pin_configure(dev, PIN_ECHO, GPIO_DIR_IN);
			gpio_pin_read(dev, PIN_ECHO, &value);
	   } while (value == 0);
    u32_t start = k_cycle_get_32();
    do {
    	    gpio_pin_configure(dev, PIN_ECHO, GPIO_DIR_IN);
			gpio_pin_read(dev, PIN_ECHO, &value); 
	   } while (value == 1);
	u32_t end = k_cycle_get_32();

    cycles_spent = end - start;
    nanoseconds_spent = SYS_CLOCK_HW_CYCLES_TO_NS(cycles_spent);
    dist_val = nanoseconds_spent/1000000.0;
    if(dist_val > 1.0)
    {
    	result = 1;   // The place is free, thereby trigger the event
    }
    else
    {
    	result = 0;   // The place is occupied
    }
	irq_unlock(key);

	return result;
}






// SImulated values for the slots state

void get_vehicle_details()
{
	
    vehicle_id = hcsr04_find_occupancy();
 
}



void read_sensor()
{
	
    
    dev = device_get_binding(PORT);
    get_vehicle_details();



}

int btn_alert_handler(struct k_alert *alert)
{
	int value;
	char payload[128];

    /* Context: Zephy kernel workqueue thread */

	printk("Button event!\n");

	gpio_pin_read(btn_dev, SIMULATED_REQUESTER_GPIO_PIN, &value);
	if (value == SIMULATED_REQUEST_SIGNAL) {
        read_sensor();
        /* Formulate JSON in the format expected by thingsboard.io */
		snprintf(payload, sizeof(payload), "{\"vehicle_id\":%d}", vehicle_id);
		tb_publish_telemetry(payload);
	}

	return 0;
}

void sensors_start()
{
	btn_dev = device_get_binding(BTN_PORT);

	gpio_pin_configure(btn_dev, SIMULATED_REQUESTER_GPIO_PIN, GPIO_DIR_IN | GPIO_INT
		| GPIO_INT_DOUBLE_EDGE | GPIO_INT_EDGE | GPIO_PUD_PULL_UP);

	gpio_init_callback(&btn_callback, btn_handler,
		BIT(SIMULATED_REQUESTER_GPIO_PIN)
	);

	gpio_add_callback(btn_dev, &btn_callback);

	gpio_pin_enable_callback(btn_dev, SIMULATED_REQUESTER_GPIO_PIN);
}
