#include <zephyr.h>
#include <board.h>
#include <device.h>
#include <gpio.h>
#include <misc/printk.h>
#include "config.h"
#include "tb_pubsub.h"
#include <stdlib.h>//required to use 'rand()'

#define BTN_PORT SW0_GPIO_NAME
#define SIMULATED_REQUESTER_GPIO_PIN SW0_GPIO_PIN
#define SLOTS_COUNT 4
#define SIMULATED_REQUEST_SIGNAL 1

u32_t slots_state[SLOTS_COUNT] = {
    0,
    0,
    0,
    0
};

u32_t vehicle_id;

struct device *btn_dev;
struct gpio_callback btn_callback;

int btn_alert_handler(struct k_alert *alert);
K_ALERT_DEFINE(btn_alert, btn_alert_handler, 10);

void btn_handler(struct device *port, struct gpio_callback *cb, u32_t pins)
{
    /* Context: interrupt handler */
	printk("Signalling alert!\n");
	k_alert_send(&btn_alert);
}

// SImulated values for the slots state

void get_vehicle_details()
{
    vehicle_id = 10;
}



void read_sensor()
{
	int r;
 	r = rand()%2;
    slots_state[0]=0;
    slots_state[1]=r;
    slots_state[2]=0;
    slots_state[3]=r;

}

int btn_alert_handler(struct k_alert *alert)
{
	int value;
	char payload[128];

    /* Context: Zephy kernel workqueue thread */

	printk("Button event!\n");

	gpio_pin_read(btn_dev, SIMULATED_REQUESTER_GPIO_PIN, &value);
	if (value == SIMULATED_REQUEST_SIGNAL) {
		get_vehicle_details();
        read_sensor();
        /* Formulate JSON in the format expected by thingsboard.io */
		snprintf(payload, sizeof(payload), "{\"vehicle_id\":%d}", vehicle_id);
		tb_publish_telemetry(payload);
	}

	return 0;
}

void sensors_start()
{
	btn_dev = device_get_binding(BTN_PORT);

	gpio_pin_configure(btn_dev, SIMULATED_REQUESTER_GPIO_PIN, GPIO_DIR_IN | GPIO_INT
		| GPIO_INT_DOUBLE_EDGE | GPIO_INT_EDGE | GPIO_PUD_PULL_UP);

	gpio_init_callback(&btn_callback, btn_handler,
		BIT(SIMULATED_REQUESTER_GPIO_PIN)
	);

	gpio_add_callback(btn_dev, &btn_callback);

	gpio_pin_enable_callback(btn_dev, SIMULATED_REQUESTER_GPIO_PIN);
}
