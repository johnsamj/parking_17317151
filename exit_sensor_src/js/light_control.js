// Customize for your thingsboard instance
var TB_ADDRESS = "localhost"
var TB_PORT = 8080

//
// You need to replace `token` below with a JWT_TOKEN obtained from your
// thingsboard instance. Follow the instructions at the URL below, specifically
// the command at the end of the page beginning `curl -X POST ...`, which you
// must modify as appropriate (thingsboard IP address in particular):
//
//   https://thingsboard.io/docs/reference/rest-api/
//
//var TB_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhcmd1ZWxsakB0Y2QuaWUiLCJzY29wZXMiOlsiVEVOQU5UX0FETUlOIl0sInVzZXJJZCI6IjA3MGVjMjIwLWM4YWItMTFlNy1iYmVmLWMzMDM1YTg5NjQ0MCIsImZpcnN0TmFtZSI6IkFkbWluIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImU3YjE2ZTUwLWM4YWEtMTFlNy1iYmVmLWMzMDM1YTg5NjQ0MCIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTUxMTk3Mjc4NywiZXhwIjoxNTIwOTcyNzg3fQ.eIzu-7pmXENslZUEV568cMvfgx3b8ALEFXUxARE3rn_R2Q-8pTfk_YZp_eell1nvUBmHhHOAlS6ron5fImiVIQ";

var TB_TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJqYWJlenNhbWpAZ21haWwuY29tIiwic2NvcGVzIjpbIlRFTkFOVF9BRE1JTiJdLCJ1c2VySWQiOiI3NDAzYjNjMC1kNWViLTExZTctYTY1Yi1jYmJlYmFlMmVlYTQiLCJmaXJzdE5hbWUiOiJKYWJleiBTYW0iLCJlbmFibGVkIjp0cnVlLCJpc1B1YmxpYyI6ZmFsc2UsInRlbmFudElkIjoiN2E1NWQ1NjAtYzJlZS0xMWU3LWI2MGUtN2ZmZGNkMzgwNTYzIiwiY3VzdG9tZXJJZCI6IjEzODE0MDAwLTFkZDItMTFiMi04MDgwLTgwODA4MDgwODA4MCIsImlzcyI6InRoaW5nc2JvYXJkLmlvIiwiaWF0IjoxNTEyMDYwNTkwLCJleHAiOjE1MjEwNjA1OTB9.TUxmE19LOzQ0U1IBfuqEqqvw2Q6B29FGthnVd2CYuX65AyaAA0jOn9h5AGldGjb8K7LokhW-z8WQUKJ3YQZZmA";


// Create an array of thingsboard DEVICE IDs corresponding to your nRF52-DKs
// You can obtain these using COPY DEVICE ID in the thingsboard web UI

// Adding two devices
var DEVICE_IDS = [
    "514504d0-c8ab-11e7-bbef-c3035a896440",
    "59682820-c2ef-11e7-b60e-7ffdcd380563"
    ];



// You might want to declare some constants to make it easier to identify
// your devices
var ENTRANCE_DEVICE = 0;
var EXIT_DEVICE = 1;


function saveExit(id) 
{
    var fs = require('fs');
    var XmlReader = require('xml-reader');
    var xml = fs.readFileSync('entry.xml', 'UTF-8');
    var ast = XmlReader.parseSync(xml);
    var xmlQuery = require('xml-query');

    //if the entry is the existing entry
    if(true)
    {
        var XMLWriter = require('xml-writer');
        var ws = fs.createWriteStream('exit.xml');
        ws.on('close', function() {
            console.log(fs.readFileSync('exit.xml', 'UTF-8'));
        });
        xw = new XMLWriter(true, function(string, encoding) {
            ws.write(string, encoding);
        });
    xw.startDocument();
    xw.startElement('vehicle_id');
    xw.text(id);
    xw.endElement('vehicle_id');
    xw.startElement('exit_time');
    xw.text(Date.now());
    xw.endElement('exit_time');
    ws.end();
    }
}

function saveEntry(vehicle_id){    
    var XMLWriter = require('xml-writer');
    fs = require('fs');
    var ws = fs.createWriteStream('entry.xml');
    ws.on('close', function() {
            console.log(fs.readFileSync('entry.xml', 'UTF-8'));
    });
    xw = new XMLWriter(true, function(string, encoding) {
            ws.write(string, encoding);
    });

    xw.startDocument();
    xw.startElement('vehicle_id');
    xw.text(vehicle_id);
    xw.endElement('vehicle_id');
    xw.startElement('enter_time');
    xw.text(Date.now());
    xw.endElement('enter_time');
    ws.end();
    
}





// Set the state of the lights on the device `deviceId`
function doLights(deviceId, lightNo, isOccupied, isSlotAssigned, vehicle_id) {

    // Use the server-side device RPC API to cause thingsboard to issue a device
    // RPC to a device that we identify by `buttonEntityId`
    // See: https://thingsboard.io/docs/user-guide/rpc/

    var request = require("request");
    var url = "http://" + TB_ADDRESS+":" + TB_PORT + "/api/plugins/rpc/oneway/" + deviceId;

    // The JSON RPC description must match that expected in tb_pubsub.c
    if (!isOccupied && !isSlotAssigned) {
        var req = {
            "method" : "putBlinkyLights",
            "params" : {
                "ledno" : lightNo
            }
        };
        isSlotAssigned = true;
        console.log("Assigned");
        saveEntry(vehicle_id);
    } else {
        var req = {
            "method" : "putLights",
            "params" : {
                "ledno" : lightNo,
                "value" : isOccupied
            }
        };
        console.log("call lights: ");
    }

    // Issue the HTTP POST request
    request({
        url: url,
        method: "POST",
        json: req,
        headers: {
            "X-Authorization": "Bearer " + TB_TOKEN,
            // Note the error in the TB docs: `Bearer` is missing from
            // `X-Authorization`, causing a 401 error response
        }
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log("OK" + ((typeof body != 'undefined') ? ": " + body : ""));
        }
        else {
            console.log("error: " + error);
            console.log("response.statusCode: " + response.statusCode);
            console.log("response.statusText: " + response.statusText);
        }
    });

    return isSlotAssigned;
}

// Process device telemetry updates received from thingsboard device `deviceId`
function processTelemetryData(deviceId, data) {

    // Note: Unfortunately the JSON parser gives us strings for the booleans
    // that we originally published from the tb_template device firmware. We
    // need to use string comparison to interpret them.

    console.log("Telemetry from " + deviceId + " : " + JSON.stringify(data));

    var isSlotAssigned = false;
    
    // Check that this is an update from the device we're interested in
    if (deviceId == DEVICE_IDS[ENTRANCE_DEVICE]) {
        // Just check for an update to button state and mirror it in the
        // corresponding LED

        //Get the Vehicle id
        if (typeof data.vehicle_id !== 'undefined') {
            var vehicle_id = data.vehicle_id[0][1];
        }
        if (typeof data.slot0 !== 'undefined') {
            isSlotAssigned = doLights(deviceId, 0, data.slot0[0][1] == "Occupied" ? true : false, false, vehicle_id);
        }
        if (typeof data.slot1 !== 'undefined') {
            isSlotAssigned = doLights(deviceId, 1, data.slot1[0][1] == "Occupied" ? true : false, isSlotAssigned, vehicle_id);
        }
        if (typeof data.slot2 !== 'undefined') {
            isSlotAssigned = doLights(deviceId, 2, data.slot2[0][1] == "Occupied" ? true : false, isSlotAssigned, vehicle_id);
        }
        if (typeof data.slot3 !== 'undefined') {
            isSlotAssigned = doLights(deviceId, 3, data.slot3[0][1] == "Occupied" ? true : false, isSlotAssigned, vehicle_id);
        }
    }

    else if (deviceId == DEVICE_IDS[EXIT_DEVICE]) {
        // Just check for an update to button state and mirror it in the
        // corresponding LED
        var vehicle_id;
        //Get the Vehicle id
        if (typeof data.vehicle_id !== 'undefined') {
            vehicle_id = data.vehicle_id[0][1];
        }

        saveExit(vehicle_id);
        
        }
    

    console.log("Finished");

}


// Use the thingsboard Websocket API to subscribe to device telemetry updates
// See: https://thingsboard.io/docs/user-guide/telemetry/

var WebSocketClient = require('websocket').client;
var client = new WebSocketClient();

client.on('connectFailed', function(error) {
    console.log('Connect Error: ' + error.toString());
});

client.on('connect', function(connection) {

    console.log('WebSocket Client Connected');

    connection.on('error', function(error) {
        console.log("Connection Error: " + error.toString());
    });

    connection.on('close', function() {
        console.log('echo-protocol Connection Closed');
    });

    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            var rxObj = JSON.parse(message.utf8Data);
            if (typeof rxObj.subscriptionId !== 'undefined') {
                processTelemetryData(DEVICE_IDS[rxObj.subscriptionId], rxObj.data);
            }
        }
    });

    // Subscribe to the latest telemetry from the device (specified by an index
    // into the DEVICE_IDS array)
    // See: https://thingsboard.io/docs/user-guide/telemetry/
    function subscribe(deviceIdx) {
        var req = {
            tsSubCmds: [
                {
                    entityType: "DEVICE",
                    entityId: DEVICE_IDS[deviceIdx],
                    scope: "LATEST_TELEMETRY",
                    cmdId: deviceIdx
                }
            ],
            historyCmds: [],
            attrSubCmds: []
        };

        console.log("Subscribing to " + DEVICE_IDS[deviceIdx]);
        connection.sendUTF(JSON.stringify(req));
    }

    if (connection.connected) {

        // Subscribe to telemetry updates for ENTRANCE_DEVICE
        subscribe(ENTRANCE_DEVICE);
        subscribe(EXIT_DEVICE);

        // Or subscribe to all if you want
        // for (deviceIdx = 0; deviceIdx < DEVICE_IDS.length; deviceIdx++) {
        //     subscribe(deviceIdx);
        // }
    }
});

client.connect("ws://" + TB_ADDRESS + ":" + TB_PORT + "/api/ws/plugins/telemetry?token=" + TB_TOKEN);
